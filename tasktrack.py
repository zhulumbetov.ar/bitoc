import requests
import time

API_ENDPOINT = "https://fapi.binance.com"
SYMBOL = "ETHUSDT"
INTERVAL = 1  # для интервала в 1 час ставим 3600 вместо 1
THRESHOLD = 0.0001  # для 1% вместо 0.0001 пишем 0.01

prev_price = None

while True:
    try:
        resp = requests.get(f"{API_ENDPOINT}/fapi/v1/ticker/price", params={"symbol": SYMBOL})
        resp.raise_for_status()
        data = resp.json()
        curr_price = float(data["price"])
        if prev_price is not None:
            price_diff = abs(curr_price - prev_price) / prev_price
            if price_diff >= THRESHOLD:
                print(f"Price changed by {price_diff:.2%}: {prev_price:.2f} -> {curr_price:.2f}")
        prev_price = curr_price
    except Exception as e:
        print(f"Error: {e}")
    time.sleep(INTERVAL)
